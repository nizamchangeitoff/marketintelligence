﻿using StatePrinting;
using System;

namespace MarketIntelligence.Models.SemRush
{
	public class OrganicSearchModel
	{
		public string Source { get; set; }
		public string FPTag { get; set; }
		public string UrlType { get; set; }
		public string Url { get; set; }

		public string DatabaseCode { get; set; }
		public string Keyword { get; set; }
		public int CurrentPosition { get; set; }
		public int SearchVolume { get; set; }
		public double CostPerClick { get; set; }
		public double Competition { get; set; }
		public double TrafficPercentage { get; set; }
		public double TrafficCostPercentage { get; set; }
		public long NumberOfResults { get; set; }
		public string Trends { get; set; }

		public int PreviousPosition { get; set; }
		public int PositionDifference { get; set; }

		public DateTime FetchedOn { get; set; }

		static readonly Stateprinter printer = new Stateprinter();

		public OrganicSearchModel(string source, string fPTag, string urlType, string url, string databaseCode, string keyword, int currentPosition, int searchVolume, double costPerClick, double competition, double trafficPercentage, double trafficCostPercentage, long numberOfResults, string trends, int previousPosition, int positionDifference, DateTime fetchedOn)
		{
			this.Source = source;
			this.FPTag = fPTag;
			this.UrlType = urlType;
			this.Url = url;
			this.DatabaseCode = databaseCode;
			this.Keyword = keyword;
			this.CurrentPosition = currentPosition;
			this.SearchVolume = searchVolume;
			this.CostPerClick = costPerClick;
			this.Competition = competition;
			this.TrafficPercentage = trafficPercentage;
			this.TrafficCostPercentage = trafficCostPercentage;
			this.NumberOfResults = numberOfResults;
			this.Trends = trends;
			this.PreviousPosition = previousPosition;
			this.PositionDifference = positionDifference;
			this.FetchedOn = fetchedOn;
		}

		public override string ToString()
		{
			return Temp(Source) + "," + Temp(FPTag) + "," + Temp(UrlType) + "," + Temp(Url) + "," + Temp(DatabaseCode) + "," + Temp(Keyword) + "," + CurrentPosition + "," + SearchVolume + "," + CostPerClick + "," + Competition + "," + TrafficPercentage + "," + TrafficCostPercentage + "," + NumberOfResults + "," + Temp(Trends) + "," + PreviousPosition + "," + PositionDifference + "," + Temp(FetchedOn);
		}
		private string Temp(string str)
		{
			if (str == null)
				return "null";

			return "\"" + str.Trim() + "\"";
		}
		private string Temp(DateTime str)
		{
			if (str == null)
				return "\"" + (new DateTime(0)).ToString("yyyy-MM-dd HH:mm:ss") + "\"";

			return "\"" + str.ToString("yyyy-MM-dd HH:mm:ss") + "\"";
		}

	}
}
