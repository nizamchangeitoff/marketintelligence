﻿using edu.stanford.nlp.tagger.maxent;
using Google.Api.Ads.AdWords.Lib;
using java.util;
using LemmaSharp;
using MarketIntelligence.Database;
using MarketIntelligence.Models;
using MongoDB.Bson.Serialization.Conventions;
using MovingFloats.API.Utils;
using MovingFloats.Server.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MarketIntelligence
{
	public static class MarketIntelligenceSummarization
	{


		// Loading POS Tagger
		static MaxentTagger tagger = null;// new MaxentTagger("english-left3words-distsim.tagger");


		public static void Main3(string[] args)
		{
			HashSet<string> seed = new HashSet<string>();
			seed.Add("website hosting");
			seed.Add("hosting");
			seed.Add("web hosting");
			seed.Add("hosting for wordpress");
			seed.Add("hosting for shopify");

			List<KeywordModel> list = GoogleAdwordsKeywordSuggestion.GetKeywordsUsingSeedKeywords(seed);
			foreach (KeywordModel km in list)
				Console.WriteLine(km.Keyword + "\t" + km.AverageMonthlySearches);

			Console.ReadLine();
		}
		public static void Main(string[] args)
		{
			ConventionRegistry.Register(typeof(IgnoreExtraElementsConvention).Name, new ConventionPack { new IgnoreExtraElementsConvention(true) }, t => true);

			if (args == null || args.Length == 0)
				args = new string[1] { "SUGGESTKEYWORDS" };

			int limitOfFPTag = 0;
			foreach (string arg in args)
			{
				if (!int.TryParse(arg, out limitOfFPTag))
					limitOfFPTag = -1;
			}
			foreach (string arg in args)
			{
				switch (arg.ToUpper())
				{
					case "SEARCHRANKING":
						Console.WriteLine("STARTING KEYWORD RANKINGS");
						UpdateKeywordRankings();
						break;
					case "PRODUCTS":
						Console.WriteLine("STARTING PRODUCTS");
						UpdateProducts();
						break;
					case "SUGGESTKEYWORDS":
						Console.WriteLine("STARTING SUGGESTED KEYWORD");
						if (limitOfFPTag <= 0)
							UpdateSuggestions();
						else
							UpdateSuggestions(limit: limitOfFPTag);
						break;
				}
				Console.ReadLine();
			}
		}



		public static Dictionary<string, string> GetFPTagPredictedCategory()
		{

			string[] fpTagPredictedCategoryArray = System.IO.File.ReadAllText("tagwithcategory.csv").Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
			Dictionary<string, string> fpTagPredictedCategoryDict = new Dictionary<string, string>();
			if (fpTagPredictedCategoryArray != null)
				foreach (string str in fpTagPredictedCategoryArray)
				{
					try
					{
						string fpTag = str.Split(',')[0];
						string predictedCategory = str.Split(',')[1];
						if (!fpTagPredictedCategoryDict.ContainsKey(fpTag))
							fpTagPredictedCategoryDict.Add(fpTag, predictedCategory);
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
						Console.ReadLine();
						Environment.Exit(0);
					}
				}

			return fpTagPredictedCategoryDict;
		}

		/* For Updating Keywords */

		public static void UpdateKeywordRankings()
		{

			Dictionary<string, string> fpTagPredictedCategoryDict = GetFPTagPredictedCategory();

			string fPListApi = "http://riamemory.withfloats.com/market/api/KeywordRank/FPList";
			WebClient wc = new WebClient();
			string fPList = wc.DownloadString(fPListApi);
			JArray partners = JArray.Parse(fPList);


			ConcurrentBag<FPMarketTrendSummaryRecord> fpMarketTrendsList = new ConcurrentBag<FPMarketTrendSummaryRecord>();

			HashSet<string> fpTagList = new HashSet<string>();

			foreach (string str in partners)
			{
				FPMarketTrendSummaryRecord fpMarketSummaryObj = new FPMarketTrendSummaryRecord();
				fpMarketSummaryObj.FpTag = str;
				if (fpTagList.Add(str))
					fpMarketTrendsList.Add(fpMarketSummaryObj);
			}

			Console.WriteLine("Finished Retrieving FP Names");

			Parallel.ForEach(fpMarketTrendsList, fpMarketSummaryObj =>
				  {
					  Parallel.Invoke(
						  () => ProcessFloatingPointFromMongo(ref fpMarketSummaryObj),
						  () => ProcessFloatingPointFromMarketSummaryDB(ref fpMarketSummaryObj));

					  string category = "";
					  if (fpMarketSummaryObj.FpCategory == "GENERAL SERVICES")
						  fpMarketSummaryObj.FpCategory = fpTagPredictedCategoryDict.TryGetValue(fpMarketSummaryObj.FpTag, out category) ? category : "GENERAL SERVICES";

					  if (string.IsNullOrWhiteSpace(fpMarketSummaryObj.FpCategory))
						  fpMarketSummaryObj.FpCategory = "GENERAL SERVICES";
				  });

			for (int i = 0; i < fpMarketTrendsList.Count; i += 200)
				InsertIntoDB(fpMarketTrendsList.Skip(i).Take(200).ToList(), TypeOfInsertion.SEARCHRANKING);
		}
		private static void ProcessFloatingPointFromMongo(ref FPMarketTrendSummaryRecord fpMarketSummaryObj)
		{
			if (fpMarketSummaryObj == null || fpMarketSummaryObj.FpTag == null)
				return;

			FloatingPointFinal fpLimitedSetObj = DBHandler.RetrieveRecordFromFloatingPointsUsingFPTag(fpMarketSummaryObj.FpTag);

			if (fpLimitedSetObj == null)
				return;

			if (fpLimitedSetObj.Category != null && fpLimitedSetObj.Category.Keys != null && fpLimitedSetObj.Category.Keys.Count >= 1)
				fpMarketSummaryObj.FpCategory = fpLimitedSetObj.Category.Keys.First();
			else
				fpMarketSummaryObj.FpCategory = "";

			fpMarketSummaryObj.FpCreatedOn = fpLimitedSetObj.CreatedOn;
		}
		private static void ProcessFloatingPointFromMarketSummaryDB(ref FPMarketTrendSummaryRecord fpMarketSummaryObj)
		{
			if (fpMarketSummaryObj == null || fpMarketSummaryObj.FpTag == null)
				return;

			Dictionary<string, int> keywordRankDict = DBHandler.GetLatestKeywordAndRankForFp(fpMarketSummaryObj.FpTag);

			fpMarketSummaryObj.LastUpdatedDate = DateTime.MinValue;

			if (keywordRankDict != null && keywordRankDict.Count > 0)
			{
				foreach (string keyword in keywordRankDict.Keys)
				{
					int rank = keywordRankDict[keyword];
					if (rank <= 10)
						fpMarketSummaryObj.FpKeywordPage1 += keyword + "," + rank + "|";
					else if (rank <= 20)
						fpMarketSummaryObj.FpKeywordPage2 += keyword + "," + rank + "|";
					else
						fpMarketSummaryObj.FpKeywordPageOther += keyword + "," + rank + "|";
				}
				if (fpMarketSummaryObj.FpKeywordPage1.EndsWith(" | "))
					fpMarketSummaryObj.FpKeywordPage1 = fpMarketSummaryObj.FpKeywordPage1.Substring(0, fpMarketSummaryObj.FpKeywordPage1.Length - 3);

				if (fpMarketSummaryObj.FpKeywordPage2.EndsWith(" | "))
					fpMarketSummaryObj.FpKeywordPage2 = fpMarketSummaryObj.FpKeywordPage2.Substring(0, fpMarketSummaryObj.FpKeywordPage2.Length - 3);

				if (fpMarketSummaryObj.FpKeywordPageOther.EndsWith(" | "))
					fpMarketSummaryObj.FpKeywordPageOther = fpMarketSummaryObj.FpKeywordPageOther.Substring(0, fpMarketSummaryObj.FpKeywordPageOther.Length - 3);
			}

		}


		/* For Adding Products */
		public static void UpdateProducts()
		{
			List<FloatingPointProductFinal> productsList = new List<FloatingPointProductFinal>();
			Dictionary<string, string> fpTagPredictedCategoryDict = new Dictionary<string, string>();
			List<string> nonExpiredFpTags = DBHandler.RetrieveUnexpiredFPTagsFromFloatingPoints();
			Console.WriteLine(nonExpiredFpTags.Count);


			Parallel.Invoke(
				() =>
				{
					fpTagPredictedCategoryDict = GetFPTagPredictedCategory();
				},
				() =>
				{
					productsList = DBHandler.RetrieveProductsFromProductsCollection(fpTags: nonExpiredFpTags.ToArray<string>());

				});

			ConcurrentDictionary<string, FPMarketTrendSummaryRecord> fpTagProductsMapping = new ConcurrentDictionary<string, FPMarketTrendSummaryRecord>();
			Parallel.ForEach(productsList, product =>
			{
				if (product == null || string.IsNullOrWhiteSpace(product.FPTag) || string.IsNullOrWhiteSpace(product.Name))
					return;

				product.FPTag = product.FPTag.ToUpper();

				string productName = product.Name?.Trim();

				if (fpTagProductsMapping.ContainsKey(product.FPTag))
				{
					if (!string.IsNullOrWhiteSpace(productName))
					{
						productName = Regex.Replace(productName.Replace(@"&amp", "and"), @"[^0-9a-zA-Z ]+", "");

						if (!string.IsNullOrWhiteSpace(productName))
							fpTagProductsMapping[product.FPTag].FpProductList = fpTagProductsMapping[product.FPTag].FpProductList + "|" + productName;
					}
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(productName))
					{
						productName = Regex.Replace(productName.Replace("&amp", "and"), @"[^0-9a-zA-Z ]+", "");
						if (!string.IsNullOrWhiteSpace(productName))
							fpTagProductsMapping.TryAdd(product.FPTag, new FPMarketTrendSummaryRecord { FpTag = product.FPTag, FpProductList = product.Name.Trim(), LastUpdatedDate = DateTime.MinValue });
					}
				}
			});
			Console.WriteLine("Retrieved Products");
			List<FPMarketTrendSummaryRecord> fpProducts = fpTagProductsMapping.Values.ToList();
			if (fpProducts != null)
				Parallel.ForEach(fpProducts, fpMarketSummaryObj =>
				{
					ProcessFloatingPointFromMongo(ref fpMarketSummaryObj);
					if (fpMarketSummaryObj.FpCreatedOn == DateTime.MinValue)
						fpProducts.Remove(fpMarketSummaryObj);

					string category = "";
					if (string.IsNullOrWhiteSpace(fpMarketSummaryObj.FpCategory))
						fpMarketSummaryObj.FpCategory = "GENERAL SERVICES";

					if (fpMarketSummaryObj.FpCategory == "GENERAL SERVICES")
						fpMarketSummaryObj.FpCategory = fpTagPredictedCategoryDict.TryGetValue(fpMarketSummaryObj.FpTag, out category) ? category : "GENERAL SERVICES";
				});

			Console.WriteLine("Merged Products and FP into MarketTrendsModel");
			Console.WriteLine("Count=" + fpProducts.Count);
			for (int i = 0; i < fpProducts.Count; i += 200)
			{
				Console.WriteLine(i);
				InsertIntoDB(fpProducts.Skip(i).Take(200).ToList(), TypeOfInsertion.PRODUCTS);
				//Task.Run(() => InsertIntoDB(fpProducts.Skip(i).Take(200).ToList(), "Products"));
			}
		}


		/* For Updating Suggestions */
		public static void UpdateSuggestions(int limit = 100)
		{
			if (limit <= 0)
				return;


			List<FPMarketTrendSummaryRecord> fpMarketTrendSummaryRecords = DBHandler.GetProductsAndKeywordRankingsForLeastRecentlyUpdatedFPs(limit);

			List<FPMarketTrendSummaryRecord> updatedSuggestionRecords = new List<FPMarketTrendSummaryRecord>();
			//Console.WriteLine(fpMarketTrendSummaryRecords.Count);
			if (fpMarketTrendSummaryRecords != null && fpMarketTrendSummaryRecords.Count > 0)
			{
				for (int i = 0; i < limit; i += 20)
				{

					List<FPMarketTrendSummaryRecord> fpMarketTrendSummaryRecordsSubset = fpMarketTrendSummaryRecords.Skip(i).Take(20).ToList();
					if (fpMarketTrendSummaryRecordsSubset != null && fpMarketTrendSummaryRecordsSubset.Count > 0)
						foreach (FPMarketTrendSummaryRecord fpMarketTrendSummaryRecord in fpMarketTrendSummaryRecordsSubset)
						{
							Console.WriteLine("FPTag:" + fpMarketTrendSummaryRecord.FpTag);
							HashSet<string> seedKeywords = new HashSet<string>();
							List<string> page1KeywordsPositions = fpMarketTrendSummaryRecord.FpKeywordPage1.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
							List<string> page2KeywordsPositions = fpMarketTrendSummaryRecord.FpKeywordPage2.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
							List<string> pageOtherKeywordsPositions = fpMarketTrendSummaryRecord.FpKeywordPageOther.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
							List<string> products = fpMarketTrendSummaryRecord.FpProductList.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();


							foreach (string keywordPosition in page1KeywordsPositions)
								seedKeywords.Add(keywordPosition.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[0]?.Trim());


							if (seedKeywords.Count >= 10)
								goto UseSeedKeywordsForSuggestion;

							foreach (string keywordPosition in page2KeywordsPositions)
								seedKeywords.Add(keywordPosition.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[0]?.Trim());

							if (seedKeywords.Count >= 10)
								goto UseSeedKeywordsForSuggestion;

							foreach (string keywordPosition in pageOtherKeywordsPositions)
							{
								string[] keywordPositionArray = keywordPosition.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
								int position = Int32.Parse(keywordPositionArray[1]);
								seedKeywords.Add(keywordPositionArray[0]?.Trim());

								if (seedKeywords.Count >= 10)
									goto UseSeedKeywordsForSuggestion;
							}

							foreach (string product in products)
							{
								seedKeywords.Add(product?.Trim());

								if (seedKeywords.Count >= 10)
									goto UseSeedKeywordsForSuggestion;
							}

							UseSeedKeywordsForSuggestion:
							foreach (string keyword in seedKeywords)
								Console.WriteLine(keyword);

							if (seedKeywords == null || seedKeywords.Count == 0)
								continue;

							int numberOfRetries = 0;
							KeywordSuggestionLabel:
							List<KeywordModel> suggestedKeywords = new List<KeywordModel>();
							try
							{
								foreach (string seedKeyword in seedKeywords)
								{

								}
								Console.WriteLine("Finding Keywords for FPTag:" + fpMarketTrendSummaryRecord.FpTag);
								suggestedKeywords = GoogleAdwordsKeywordSuggestion.GetKeywordsUsingSeedKeywords(seedKeywords);
								fpMarketTrendSummaryRecord.LastUpdatedDate = DateTime.UtcNow;
							}
							catch (AdWordsException adwordsException1)
							{
								Console.WriteLine("Failed to retrieve related keywords." + adwordsException1.Message);
								if (!adwordsException1.Message.Contains("RATE_EXCEEDED"))
									Console.WriteLine(adwordsException1);

								Thread.Sleep(2500);
								numberOfRetries++;
								if (numberOfRetries < 2)
									goto KeywordSuggestionLabel;

							}
							catch (Exception e)
							{
								Console.WriteLine(e);
								Thread.Sleep(2500);
								numberOfRetries++;
								if (numberOfRetries < 2)
									goto KeywordSuggestionLabel;
							}

							if (suggestedKeywords != null && suggestedKeywords.Count > 0)
							{
								fpMarketTrendSummaryRecord.FpSuggestedKeywords = "";
								foreach (KeywordModel keywordModelObj in suggestedKeywords)
								{
									if (string.IsNullOrWhiteSpace(fpMarketTrendSummaryRecord.FpSuggestedKeywords))
										fpMarketTrendSummaryRecord.FpSuggestedKeywords = keywordModelObj.Keyword;
									else
										fpMarketTrendSummaryRecord.FpSuggestedKeywords += "|" + keywordModelObj.Keyword;
								}
							}
						}

					InsertIntoDB(fpMarketTrendSummaryRecordsSubset, TypeOfInsertion.SUGGESTKEYWORDS);
				}
			}
		}



		public static void UpdateSeedkeywords()
		{

		}
		/* For Google Toolbar Suggestion */

		public static void Main2(string[] args)
		{
			/*
			DateTime startDate = new DateTime(2012, 01, 01, 0, 0, 0, DateTimeKind.Utc);
			DateTime endDate = new DateTime(2016, 12, 01, 0, 0, 0, DateTimeKind.Utc);

			Console.WriteLine("DATERANGE:: " + startDate + "-->" + endDate);
			//Console.ReadKey();
			List<FloatingPointFinal> fpList = DBHandler.RetrieveRecordsFromFloatingPointsInDateRange(startDate, endDate);
			if (fpList != null && fpList.Count > 0)
			{
				foreach (FloatingPointFinal fp in fpList)
				{
					string fPCategory = null;
					if (fp.Category != null && fp.Category.Keys != null && fp.Category.Keys.Count >= 1)
						fPCategory = fp.Category.Keys.First();

					FPMarketTrendSummaryRecord fpMarketSummaryObj = new FPMarketTrendSummaryRecord(fp.Tag, fp.CreatedOn, fPCategory);

					//	fpMarketSummaryObj.FpProductList
				}
			}
			*/
			SearchSuggestionsApi searchApi = new SearchSuggestionsApi();

			List<GoogleSuggestion> list = searchApi.GetSearchResults("used cars hyderabad");
			foreach (GoogleSuggestion gs in list)
				Console.WriteLine(gs.Phrase);

			Console.ReadLine();
		}
		public static void Temp()
		{


			string[] exampleWords = null; //exampleSentence.Split(new char[] { ' ', ',', '.', ')', '(' }, StringSplitOptions.RemoveEmptyEntries);

			ILemmatizer lmtz = new LemmatizerPrebuiltCompact(LemmaSharp.LanguagePrebuilt.English);

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Example sentence lemmatized");
			Console.WriteLine("        WORD ==> LEMMA");


			string exampleSentence = "angola recruitment agencies,faisal communication,lavana interiors,little nap recliners,preethi hospital madurai,corti hearing clinic,kenya recruitment agencies,recruitment in kenya,job search kenya,kenya recruitment,manpower recruitment kenya,jobs search kenya,recruitment agencies in kenya,manpower jobs kenya,agencies in kenya,mangalore jewellers,mangalore jewellery,venus canteen equipments,vacuum pump supplier,vacuum pump suppliers,vacuum pump manufacturers,vacuum pumps manufacturers,vacuum pump manufacturer,ultra high vacuum pump,industrial vacuum pumps manufacturers,vacuum needle valve,high vacuum pumps manufacturers,vacuum pumps manufacturer,refrigerator vacuum pump,vacuum pump companies,vacuum pump supplier,vacuum pump suppliers,vacuum pump manufacturers,vacuum pumps manufacturers,vacuum pump manufacturer,vacuum pumps suppliers,vacuum engineering,ultra high vacuum pump,industrial vacuum pumps manufacturers,vacuum needle valve,high vacuum pumps manufacturers,vacuum pumps manufacturer,refrigerator vacuum pump,vacuum pump companies,shiv clinic,home foods salem,ideal datacom,datacom.com,indulge the salon,indulged by beauty,madurai siva photos,panacya,the slimming centre,popular jewellers,skb engineering systems,tandoor stone,tandur andhra pradesh,chandrakala theatre moosapet online booking,utopia holidays,a.h.taher & co,deepam cabs bangalore,boys hostel in delhi,jain boys,mj associates,mobility aids sales & services,teksons bookshop,ss grill,celebrations jubilee hills,celebrations restaurant hyderabad,villa in hyderabad,discount blood tests,discount dial,call 011,junior tattoos,sail tmt,reputed organization,tmt stands for,neo steel,neosteel,hr plates,shree tmt,cr sheets,gemini 500,jsw tmt plus,manmohan sharma,mms solutions,monginis the cake shop,mongini cakes,cake shops in thane,cake shop in thane,cake shop monginis,monginis cake shop,monginis cake,monginis cakes,monginis,monginis thane,road safety signages,road safety sign boards,road delineators,road delineator,best roofing company,chennai weather condition,roofing contractors,best polycarbonate roofing,ik engineering,musaddilal jewellers,musaddilal jewellers hyderabad,musaddilal jewellers basheerbagh,akshar tour and travels,akshar tours and travels,akshar travels ahmedabad,aone sms,glorious enterprises,kepro tools,mobile shop display,mobile shop in ahmedabad,mobile shops in ahmedabad,mobile stores in ahmedabad,mobile ahmedabad,supermarket racks manufacturer,vegetable rack,supermarket racks manufacturers,departmental store racks,supermarket rack manufacturers,mobile store ahmedabad,patanjali store,nim pune,shri infotech,guasa treatment,guasa treatment,madurai street map,sclerology diagnosis,height increase clinic,annamalai university madurai,height increase treatment,dhinamani,thinamani,madurai in india map,blazon software,britto design,gem cooling tower,gem equipment,mb corporation,over the flames menu,radheshyam travels,scigenics biotech,foam pu,pu foam sheet,smart kidz toys,soundkraft,exide battery dealers in hyderabad,microtek inverter with exide battery price,inverters in hyderabad,exide inverters,exide battery details,microtek inverter price in hyderabad,exide battery price in hyderabad,exide inverter battery,exide battery dealer in hyderabad,luminous inverter hyderabad,trophy dealers,skin specialist in jaipur,skin doctor in jaipur,skin specialists in jaipur,skin specialist jaipur,skin dr in jaipur,atul jain,gem equipments,gpsp sheet,gpsp sheet,gurgaon industrial directory,gupta map,friendship club in lucknow,mr jatt music,allmusic.in,allmusic.in,allmusic.in,pvt music,allmusic yes,nippon furniture,ashoka packers and movers,plastic crates manufacturers,croma timings,abb ahmedabad,abb mccb,abb contactor,abb soft starter,abb dealer,abb distribution board,adeva spa,guitar classes in south delhi,guitar school in delhi,guitar in delhi,guitar classes in noida,guitar classes in delhi,guitar lessons in delhi,hari om travels,om travels,tamil printed t shirts,chennai t shirts,conveyor belt manufacturers in delhi,sarlas beauty parlour,sarlas beauty parlour,sarlas beauty parlour,tailorbird clothing,shake maker,shake makers,tradelink agency,tradelink agency,tradelink agency,tradelink products,luxury ceiling fans,designer ceiling fan,ceiling fans designs,designer fans india,decorative ceiling fans india,designer fans in india,crompton greaves decorative ceiling fans,designs for ceiling,ceiling fans with lights india,designer ceiling fans,ceiling fan designs,luxury ceiling fans india,usha designer fans,designer ceiling fans in india,latest ceiling fan designs,design of ceiling fan,ceiling fan usha,unique ceiling fans,crompton greaves designer fans,usha decorative ceiling fans,designer ceiling fans india,designer fans,luxury ceiling fans with lights,designer fans for ceiling,mop buttons,best mop in india,shell india chennai,sea shell buttons,shell india bangalore,mop manufacturer,button supplier,mop shell,different types of buttons,button manufacturer,buttons manufacturers,natural shell buttons,d home,dtdc courier bhiwandi,dtdc courier bhiwandi contact no,ferrule printing machine,ferrule machine,focus iit jee,link packers and movers,gati surface,gati movers and packers bangalore,gati packers and movers in delhi,genova biotech,car accessories hyderabad,hertz hyderabad,car accessories in hyderabad,elegant car seat covers,autoform seat covers,plast mart,linger coorg,aipmt delhi,pmt coaching in delhi,best physics,physics aipmt,physics tutor,physics for aipmt,best medical coaching in delhi,best iit jee coaching in delhi,pmt delhi,pmt classes,delhi pmt,physics tuition,teacher in delhi,best coaching for medical in delhi,aipmt physics,tuition in delhi,self teaching physics,iit delhi physics,trophy dealers,mementoes,trophy manufacturer,buy trophy,trophy manufacturers,cricket trophy price,brass trophy,mementos designs,trophy shops,star trophies,trophy wholesalers,plot in chandigarh,retirement trophy,oldest trophy,hand trophy,trophy supplier,mementoes design,brokeraging,roja flowers,ecotourism in tamilnadu,picasa saree,picasa sarees,corporate gifts ahmedabad,correspondence college,bangalore correspondence college,correspondence college in bangalore,gi conduit pipe,mtr chennai,shiv enterprise,shobha products,shobha products,sobha properties,johnson sanitary ware,thermocol plate making machine manufacturer,plastic disposal glass machine,thermocol plate machine price,thermocol plates making machine,paper disposal machine,disposable plastic glass making machine,thermocol glass making machine,plastic glass making machine,disposable paper plates making machine,thermocol plate making machine,thermocol raw material,ace-classes.com,guasa therapy,guasa treatment,flats in tambaram,flat promoters in chennai,chennai flat promoters,chennai flats for sale,plot promoters in chennai,good earth school tambaram,flats near tambaram,promoters in chennai,chennai flats sale,flats for sale in tambaram,flat for sale chennai,ready to occupy flats,plots near tambaram,plots near tambaram,flat for sale in chennai,nebosh cochin,iosh course in india,nebosh diploma course,nebosh institute in india,nebosh course in kerala,nebosh course in kerala,nebosh courses in kerala,nebosh in kerala,nebosh in kerala,safety officer jobs in kerala,iadc rig pass,nebosh fees,h2s training requirements,british safety council diploma,hse jobs abroad,nebosh vacancies,nebosh igc india,ashei,nebosh exam fees,h2s awareness training,av properties coimbatore,need look no further,restaurants in visakhapatnam,vizag to goa,visakhapatnam restaurants,restaurants in vizag,vizag restaurants,sheer happiness,iti courses delhi,iti ncvt,iti delhi courses,iti coures,uttar pradesh iti,gyan image,industrial training institute delhi,iti in noida,iti courses,ncvt courses,heavens tattoo,ikon electronics,india first robotics,hydraulic filter press,jayco products,fully automatic filter press,filter press manufacturer in india,membrane filter press,jayco spares,industrial filter press,jayco industries,hydraulic filter press manufacturers,best possible manner,kv institute of management coimbatore,kv institute of management,drop saree,expensive sarees,food ingredients india,ayurvedic treatment delhi,ucmas abacus mental arithmetic,park balluchi,mrs india,tonk road,manoharlal jewellers,nirvana spa treatments,park plaza spa,lite bite,assam food,apeejay surrendra group,ccc culinary,lord of the drinks,where is malshej ghat,rajastan capital,burger king india website,kokan darshan trip,big fish app,ucmas usa,vanilla moon,burger king delhi,tamara coorg resort,ccc chef,uc mas,7 gun salute,executive chef profile,ucmas abacus,urbannirvana,dmart,raod,resorts in mandarmani,bk guest experience,big boss hand,engine cleaning solution,courtyard mumbai,the tamara,1st navratra,abacus and mental arithmetic,garam dharam,rose valley mandarmani,kerala tourism mart,food of assam,nau store,marriot mumbai,kitchen & bath show,ucmas,rajeev varman,burger king delhi india,green eye glasses,praneet singh,burger king combo prices,malshej ghats,exa india,vintage cars rally,india burger king,kolkata maa durga,contempo interiors,vanilla moon shoes,frozen blanket,the tamara coorg resort,tama,ptc aviation academy in chennai,air hostess interview,best air hostess academy,airlines jobs in chennai,male air hostess,air hostess academy,aviation courses in bangalore,airlines job in chennai,how to become a male air hostess,air hostess course in india,ptc bangalore,software training institutes in kolkata,sri laxmi enterprises,laxmi enterprises,buy silicone wristbands,leather wristbands online,wristband online shopping,wristband makers,buy wristbands online,wristband online shopping in india,rubber bands online india,wristband buy online,leather wristbands online india,custom rubber wristbands,sports wristband online,online wrist bands,online wristbands,sports wristbands online,buy rubber bands online india,wrist bands online,wristband manufacturers,livestrong type bracelets,coloured rubber wristbands,nike silicone wristbands,nike rubber wristband,nike silicone bands,nike rubber wristbands,make wristbands online,silicone wristbands nike,nike silicone bracelet,nike silicone bracelets,megadeth wristband,fastrack wrist bands,customized livestrong bracelets,silicone wristbands canada,personalized rubber bracelets no minimum,nike silicone wristband,nike wristband rubber,carz,click.in coimbatore,benten photos,images of ben ten,wilo pumps,butterfly valve distributor,kitz valves distributors,butterfly valve kitz,bubbles hair salon hyderabad,modern lady dress,ladies suit neck,buy dream catchers online,where can i buy dream catchers,buy dreamcatchers online,dream catchers online,dreamcatchers online,dream catcher buy online,where to buy dreamcatchers online,dream catchers buy online,where do you buy dream catchers,dreamcatchers india,international courier delhi,dhl courier services in delhi,courier services in delhi,best international courier service,dhl gurgaon,dhl noida,fedex courier noida,courier service in delhi,fedex courier service in delhi,dhl courier service delhi,dhl courier gurgaon,fedex courier in delhi,courier service delhi,international courier service in delhi,courier services delhi,international courier companies,dhl courier noida,delhi courier services,international courier services delhi,delhi courier service,fastest courier service in delhi,best courier services in delhi,dhl courier in delhi,dhl courier delhi,fastest international courier,courier companies in delhi,fedex courier contact number delhi,dhl express delhi,international courier services in gurgaon,courier services in south delhi,best courier service in delhi,international courier in delhi,international courier company in delhi,dhl courier service in delhi,international courier services in delhi,madras flying club,microtech solutions,microtech dealers,top seo company in delhi ncr,seo company delhi ncr,sme companies in delhi,seo company in ncr,website promotion company in delhi,best seo company in delhi ncr,website promotion in delhi,open arms international,international schools in tamilnadu,central board secondary education,central board for secondary education,pet shop in south delhi,rottweiler puppies in delhi,birds shop in delhi,playfactory,the play factory,play factory,riddhi pharma,shibori designer,designer blouse boutique,fashion designing course in chennai,beautician training,stitching classes,tailoring courses,boutique designer blouse,fashion design blouse,different blouses,bridal blouses,bridal designer blouses,omr map,designer blouses in chennai,blouse stitching in chennai,designer blouses chennai,types of blouses,stainless steel modular kitchen,modular kitchen delhi,modular kitchen delhi manufacturer,modular kitchen manufacturers delhi,steel modular kitchen,kitchen manufacturers in delhi,modular kitchen in delhi,modular kitchens delhi,modular kitchen stainless steel,modular kitchen manufacturer in delhi,stainless steel modular kitchen accessories,new modular kitchen,modular kitchen details,modular kitchens in delhi,modular kitchen manufacturers in delhi,stainless steel modular kitchen manufacturers,modular kitchen steel,nursing colleges in gurgaon,nursing colleges in delhi ncr,post bsc nursing,nursing institute in delhi,institute in gurgaon,personalized keychains wholesale,keychains wholesale,promotional key chains,keychain wholesale,business keychains wholesale,promotional keychains wholesale,interiors in hyderabad,interior designer in hyderabad for home,interior in hyderabad,hyderabad interiors,best interiors in hyderabad,interiors hyderabad,best interior designer in hyderabad,new tv unit,kitchen by design hyderabad,best room interiors,www.balaji,www.balaji.org,balaji & co,ca coaching ahmedabad,food world hyderabad,hms physiotherapy equipments,hms medical systems,pocket tens unit,hms physiotherapy equipments,tiruvannamalai website,bicycle shops in delhi,cycle stores,designer candles,google promotion in delhi,seo company in dwarka,sj industries,sjindustries,aluminium fabricators,scholar hub,ammonia printing machine,a0 drawing size,a1 drawing size,drawing hangers,plan hangers,amentis,bakery raw materials,raw material for chocolate,paper plate raw material suppliers,raw material supplier,bakeware suppliers,nebosh cochin,nebosh cochin,nebosh igc in india,nebosh courses in india,nebosh training in india,nebosh course in kerala,food safety courses in india,nebosh india,nebosh courses in kerala,nebosh in kerala,canara bank branch timings,nebosh india,indian cockroach,cockroach dreams,indian cockroach,pci pest control,grace charitable trust,seo services local delhi,local seo services delhi,herbal hair treatment,herbal hair care,interior designers in dwarka,best interior designers,best interior designer,interior designer in dwarka,rubber stamp making machine manufacturers,buy house in ahmedabad,rubber stamp material,rubber stamp machine price list,self inking stamp online india,stamp making machine price,pre inked stamps,pre ink stamp,rubber stamp making machine cost,rubber stamp making machine,buy a house in ahmedabad,stamp suppliers,rubber stamp machine price,rubber stamp machine suppliers,shiny stamps,shiny stamp,stamp making machine,rubber stamp machine manufacturers,house in ahmedabad,jayanth kalamkari,kalamkari designs,jayanth kalamkari sarees,baby care products manufacturers,mosquito net bed,babycare india,baby care products india,breast pump in india,mosquito net for bed in chennai,mosquito net for bed chennai,breast pump india,bed with net,new born baby shops in chennai,botties,sunbaby products,baby products supplier,cereal feeder,baby cereal feeder,mosquito net for bed in chennai,born baby cartoon,breast pump india,masala powder manufacturers,best roofing sheets,polycarbonate sheets in chennai,construction material cost in chennai,aluminium roofing sheet price,quality roofing,building material cost in chennai,roofing sheet price,polycarbonate sheet chennai,mangalore tile roofing,roofing shed,pvc roofing sheets price,best roofing company,roofing sheets manufacturers in chennai,terrace roofing,polycarbonate sheet in chennai,polycarbonate sheet suppliers in chennai,residential roofing contractors,metal roofing contractors,metal roofing sheets price,roofing sheets chennai,polycarbonate sheet manufacturers in chennai,chennai weather condition,construction materials cost in chennai,industry norms,roofing contractors,turnkey roofing,quality roofs,insulated roofing contractors,no1 roofing,companies in sriperumbudur,no 1 roofing,chennai rainy season,fancy numbers in aircel,aircel online recharge postpaid bill payment,prepaid fancy numbers,fancy prepaid numbers,aircel postpaid payment,aircel postpaid plan,aircel fancy numbers,aircel postpaid plans,aircel postpaid,aircel bill payment,bill payment of aircel,postpaid bill payment aircel,bill payment aircel,aircel bill payment postpaid,aircel post paid plans,aircell postpaid,aircell postpaid,aircel tarrif,payment aircel,www.aircel.com.in,ribbons and balloons kandivali east,ribbons and balloons kandivali,ginger garlic paste manufacturers,ginger garlic paste suppliers,sln enterprises,bluetooth banyan,spy bluetooth pen,bluetooth earpiece spy,bluetooth invisible earpiece,spy gadgets in bangalore,spy bluetooth watch earpiece set,spy bluetooth earpiece,spy bluetooth banyan with earpiece,spy bluetooth devices,spy camera in bangalore,nano earpiece,spy banyan,spy bluetooth neckloop earpiece,sp road bangalore map,spy camera bangalore,spy bluetooth banyan,spy bluetooth banyan earpiece,spy cameras in bangalore,spy bluetooth pen earpiece set,spy pen camera in bangalore,spy bluetooth,spy camera dealers in bangalore,spy bluetooth device,spy zone,spy bluetooth earpiece set,gsm earpiece,sudhanshu shekhar,private detective agencies in kerala,dressshop,wedding gowns in chennai,dresses in chennai,stepper motor supplier,best solar lights,cctv camera distributors,cctv camera chandigarh,cctv camera distributor,cctv camera dealers,which solar panels are best quality,solar street light specification,garden street lights,solar street light price list,anil semiya,dance academy in gurgaon,dance academy gurgaon,best keyboard india,vocal classes,kathak dance music,dance school in gurgaon,art and craft classes,classical kathak dance,enjoy masti,dance classes in gurgaon,rajiv sir,western dance videos for kids,cat coore,kathak youtube,learning kathak,ballet classes in gurgaon,lucknow gharana,pcb coating spray,contact cleaner spray,temperature in gujarat,ahmedabad temperature,gujarat it,vertrel,pcb spray coating,carbon cleaner spray,temp ahmedabad,gujarat temperature,temperature of gujarat,darien electric,guru furniture,professional interior designer,power tools dealers,ideal power tools,lot hyderabad,novel business center,zicom access control system,ceiling designs for homes india,www.housebeautiful.com,homesecurity.honeywell.com,best recording package,best wardrobe designs,gift box manufacturers,box manufacturers,fireworks in sivakasi,sivakasi fireworks price list,sangam city allahabad,sushant city,sushant city lucknow,studio 9 salon faridabad,sunshine garden boutique,universal mobiles,italia glass mosaic,dolphin tiles,utsav boutique,modular kitchen trichy,vimi medi spa,maruti showroom in chennai,maruti car showroom,maruticar,chennai maruti dealers,maruti cars in chennai,maruti dealers in chennai,acupressure instrument,ammonia chiller manufacturers,aqua tech plus,aquatech plus,leather safety shoes,best safety shoes in india,safety shoes manufacturers in agra,safety boots india,best boots in india,safety shoes manufacturers in kanpur,leather safety shoes manufacturers,safety shoe manufacturers in india,safety shoes manufacturers in chennai,industrial safety shoes manufacturers in india,cctv camera services,cctv camera in ahmedabad";

			//exampleWords = exampleSentence.Split(new char[] { ' ', ',', '.', ')', '(' }, StringSplitOptions.RemoveEmptyEntries);
			exampleWords = exampleSentence.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

			HashSet hs = new HashSet();
			foreach (string phrase in exampleWords)
			{
				string[] allWords = phrase.Split(new char[] { ' ', ',', '.', ')', '(' }, StringSplitOptions.RemoveEmptyEntries);
				string str = "";
				foreach (string word in allWords)
					str += LemmatizeOne(lmtz, word) + " ";

				hs.add(str);
			}



			Console.WriteLine("\n\n");
			foreach (string str in hs)
				Console.Write(str + ",");

			Console.ForegroundColor = ConsoleColor.White;

			Console.ReadLine();

		}
		private static string LemmatizeOne(LemmaSharp.ILemmatizer lmtz, string word)

		{

			string wordLower = word.ToLower();

			string lemma = lmtz.Lemmatize(wordLower);

			Console.ForegroundColor = wordLower == lemma ? ConsoleColor.White : ConsoleColor.Red;

			string str = tagger.tagString(wordLower);


			if (str.Substring(str.LastIndexOf('_') + 1).StartsWith("NN"))
			{
				Console.WriteLine("{0,12} ==> {1} \t\t{2}", word, lemma, str);
				return wordLower;
			}
			return "";

		}





		private static int InsertIntoDB(List<FPMarketTrendSummaryRecord> fpSummaryList, TypeOfInsertion mode)
		{
			Console.WriteLine(fpSummaryList.Count);
			if (fpSummaryList == null || fpSummaryList.Count == 0)
				return -1;

			int count = fpSummaryList.Count;

			string query;

			int offset = 0;
			int limit = 500;
			int numOfRecordsInserted = 0;
			while (offset < count)
			{
				query = "INSERT INTO organicKeywordResearchSummary(fp_tag, fp_createdOn, fp_category, fp_keywordBag, fp_productList, fp_keyword_page1, fp_keyword_page2, fp_keyword_pageOther, fp_seed_keywords, fp_suggestedKeywords, last_update_date) VALUES ";


				int endPoint = offset + limit;
				if (endPoint > count)
					endPoint = count;

				for (int i = offset; i < endPoint; i++)
				{
					if (string.IsNullOrWhiteSpace(fpSummaryList.ElementAt(i).FpTag))
						continue;

					query += "(" + fpSummaryList.ElementAt(i) + "),";

				}
				offset = endPoint;

				if (query.EndsWith("VALUES "))
					return -1;

				if (query.EndsWith(","))
					query = query.Substring(0, query.Length - 1);

				query += " ON DUPLICATE KEY UPDATE ";

				switch (mode)
				{
					case TypeOfInsertion.SEARCHRANKING:
						query += "fp_category=VALUES(fp_category),fp_keyword_page1=VALUES(fp_keyword_page1), fp_keyword_page2=VALUES(fp_keyword_page2), fp_keyword_pageOther=VALUES(fp_keyword_pageOther)";
						break;
					case TypeOfInsertion.PRODUCTS:
						query += "fp_productList=VALUES(fp_productList)";
						break;
					case TypeOfInsertion.SUGGESTKEYWORDS:
						query += "fp_suggestedKeywords=VALUES(fp_suggestedKeywords),last_update_date=VALUES(last_update_date)";
						break;
				}

				//Console.WriteLine(query);
				numOfRecordsInserted += DBHandler.ExecuteMySqlInsertQuery(query);
				Console.WriteLine("Total Records Processed:" + numOfRecordsInserted);

				//Console.WriteLine(query);

			}

			//Console.ReadLine();
			return numOfRecordsInserted;


		}

		public enum TypeOfInsertion
		{
			SEARCHRANKING,
			PRODUCTS,
			SUGGESTKEYWORDS
		}
	}

}
